const db = require('../config/db.config');
const User = db.users;

// Post a Customer
// exports.create = (req, res) => {	
// 	// Save to PostgreSQL database
// 	Customer.create({
// 				"firstname": req.body.firstname, 
// 				"lastname": req.body.lastname, 
// 				"age": req.body.age,
// 				"test": req.body.test
// 			}).then(customer => {		
// 			// Send created customer to client
// 			res.json(customer);
// 		}).catch(err => {
// 			console.log(err);
// 			res.status(500).json({msg: "error", details: err});
// 		});
// };
 
// FETCH All Customers
exports.findAll = (req, res) => {
	User.findAll().then(users => {
			res.json(users.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// Find a Customer by Id
exports.findById = (req, res) => {	
	User.findById(req.params.id).then(users => {
			res.json(users);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Update a Customer
exports.update = (req, res) => {
	const id = req.body.id;
	User.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "Updated Successfully -> user Id = " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Delete a Customer by Id
exports.delete = (req, res) => {
	const id = req.params.id;
	User.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Deleted Successfully -> user Id = ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
import { Routes } from '@angular/router';
import { LoginComponent } from './authorization/login/login.component';
import { AppComponent } from './app.component';
import { TitleComponent } from './title/title.component';

export const ROUTES: Routes = [
    { path: '',      component: TitleComponent },
    { path: 'login',  component: LoginComponent },
];
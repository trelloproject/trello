import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { AuthorizationModule } from './authorization/authorization.module';
import { TitleComponent } from './title/title.component';
import {MaterialModule} from './material';

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
  ],
  imports: [
    RouterModule.forRoot(
        ROUTES
    ),
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AuthorizationModule
  ],
  exports     : [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
